/**
 * Team Fabião App Versão 1.0
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  WebView
} from 'react-native';


class TeamFabiao extends Component {

  render() {
    return (
      <View style={styles.container}>
        <WebView source={{ uri: 'https://wildflydev-teamfabiaoapp.rhcloud.com/checkin'}}
        javaScriptEnabled={true}
        domStorageEnabled = {true}

        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },

});

AppRegistry.registerComponent('TeamFabiao', () => TeamFabiao);
